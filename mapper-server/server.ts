import express from 'express'
import cors from 'cors'
import http from 'http'
import expressSession from 'express-session'
import * as runningAt from 'listening-on'
import {join, resolve} from 'path'
import {config} from 'dotenv'
import {router} from './router'
import SocketIO, {Socket} from 'socket.io'
import {SocketSession} from "./models/SocketSession";
import debug from "debug";

config()

const log = debug('App:Server')

const app = express()
app.use(cors())
const server = http.createServer(app)
const io = new SocketIO.Server(server, {
    cors: {
        origin: 'http://localhost:3000',
        methods: ['GET', 'POST'],
    },
})

app.use(express.static('public'))

app.use(express.json() as any)
app.use(express.urlencoded({extended: true}) as any)

const session = expressSession({
    secret: process.env.SESSION_SECRET || Math.random().toString(36),
    saveUninitialized: true,
    resave: true,
})
app.use(session)

app.use((req, res, next) => {
    if (req.method === 'GET') {
        log(req.method, req.url)
    } else {
        log(req.method, req.url, req.body)
    }
    next()
})

app.use(router)

app.use((req, res) => {
    res.status(404).sendFile(resolve(join('public', '404.html')))
})

io.on('connection', socket => {
    let session = new SocketSession(socket)
})

const PORT = +process.env.PORT! || 8100

server.listen(PORT, () => {
    const realLog = console.log
    console.log = log
    runningAt.print(PORT)
    console.log = realLog
})
