import {createDB} from 'compact-db'

export const db = createDB({path: 'data'})
