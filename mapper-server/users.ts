import express from 'express'
import {User} from "./models/User";
import {Chatroom} from "./models/Chatroom";
import {type} from "os";

let router = express.Router()

export default router

router.post('/users', (req, res) => {
    if (!req.body.username) {
        res.status(400).json({error: 'missing username'})
        return
    }
    let user = User.register(req.body.username)
    res.json({user})
})

router.get('/users', (req, res) => {
    res.json({users: User.getUsers()})
})

router.get('/users/:username/chat', (req, res) => {
    if (!req.params.username) {
        res.status(400).json({error: 'missing username'})
        return
    }
    const user = req.header('X-User')
    if (!user && typeof user !== "string") {
        res.status(401).json({error: 'not login'})
    }
    let from = User.load(user!)
    let to = User.load(req.params.username)
    let messages = Chatroom.getMessages(from, to)
    res.json({messages})
})


router.post('/users/:username/chat', (req, res) => {
    if (!req.params.username) {
        res.status(400).json({error: 'missing username'})
        return
    }
    const user = req.header('X-User')
    if (!user && typeof user !== "string") {
        res.status(401).json({error: 'not login'})
        return;
    }
    if (!req.body.text) {
        res.status(400).json({error: 'missing text'})
        return;
    }
    let from = User.load(user!)
    let to = User.load(req.params.username)
    Chatroom.newMessage(from, to, req.body.text)
    res.json('ok')
})
