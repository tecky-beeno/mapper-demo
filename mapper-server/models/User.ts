import {db} from "../db";
import {randomUUID} from "crypto";

export class User {
    private static usersByUsername = new Map<string, User>()
    private static usersById = new Map<string, User>()

    static login(username: string): User {
        if (this.usersByUsername.has(username)) {
            return this.usersByUsername.get(username)!
        }
        return this.register(username)
    }

    static load(username: string): User {
        if (this.usersByUsername.has(username)) {
            return this.usersByUsername.get(username)!
        }
        throw new Error('user not found')
    }

    static register(username: string): User {
        for (; ;) {
            let id = randomUUID()
            if (this.usersById.has(id)) {
                continue
            }
            let user = new User(id, username)
            this.addUser(user)
            db.set('users', this.getUsers())
            return user
        }
    }

    static getUsers() {
        return Array.from(this.usersByUsername.values())
    }

    static addUser(user: User) {
        User.usersByUsername.set(user.username, user)
        User.usersById.set(user.id, user)
    }

    private constructor(public id: string, public username: string) {
    }
}

((db.get('users') || []) as User[])
    .forEach(user => User.addUser(user))
