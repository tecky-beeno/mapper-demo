import {User} from "./User";
import {db} from "../db";
import {SocketSession} from "./SocketSession";
import {randomUUID} from "crypto";

export type StoredChatMessage = {
    id: string
    from_user_id: string
    to_user_id: string
    text: string
}
export type UIChatMessage = {
    id: string
    from_username: string
    to_username: string
    text: string
}

export class Chatroom {
    static messageList: StoredChatMessage[] = []
    static usedId = new Set<string>()

    private static addMessage(message: StoredChatMessage) {
        this.usedId.add(message.id)
        this.messageList.push(message)
    }

    static init() {
        ((db.get('messages') || []) as StoredChatMessage[]).forEach(msg => Chatroom.addMessage(msg))
    }

    static newMessage(from: User, to: User, text: string) {
        let id = ''
        for (; ;) {
            id = randomUUID()
            if (this.usedId.has(id)) {
                continue
            }
            break
        }
        this.addMessage({
            id,
            from_user_id: from.id,
            to_user_id: to.id,
            text
        })
        db.set('messages', this.messageList)
        let uiChatMessage: UIChatMessage = {
            id,
            from_username: from.username,
            to_username: to.username,
            text
        }
        SocketSession.allSessions.forEach(session => {
            if (session.auth?.username === from.username ||
                session.auth?.username === to.username) {
                session.emit('chatMessage', uiChatMessage)
            }
        })
    }

    static getMessages(from: User, to: User): UIChatMessage[] {
        let messages: UIChatMessage[] = []
        this.messageList.forEach(message => {
            [[from, to], [to, from]].forEach(([from, to]) => {
                if (message.from_user_id === from.id && message.to_user_id === to.id) {
                    messages.push({
                        id: message.id,
                        from_username: from.username,
                        to_username: to.username,
                        text: message.text
                    })
                }
            })
        })
        return messages
    }
}

Chatroom.init()
