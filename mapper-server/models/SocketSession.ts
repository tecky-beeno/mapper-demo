import {Socket} from "socket.io";
import debug from "debug";

const log = debug('App:SocketSession')

type Auth = {
    id: string
    username: string
}

export class SocketSession {
    static allSessions = new Set<SocketSession>()
    auth?: Auth

    constructor(private socket: Socket) {
        SocketSession.allSessions.add(this)
        log('client connected:', socket.id)
        socket.on('close', this.onClose)
        socket.on('auth', this.onAuth)
    }

    onClose = () => {
        log('client disconnected:', this.socket.id)
    }

    onAuth = (auth: Auth) => {
        log('received auth:', auth)
        this.auth = auth
    }

    emit(event: string, data: any) {
        this.socket.emit(event, data)
    }
}
