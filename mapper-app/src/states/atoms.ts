import { atom } from 'recoil'

export type Auth = {
  id: string
  username: string
}

export const authAtom = atom<Auth | null>({
  key: 'auth',
  default: null,
})

export type SocketState = {
  ready: boolean
  connecting: boolean
}
export const socketAtom = atom<SocketState>({
  key: 'socketState',
  default: { ready: false, connecting: false },
})
