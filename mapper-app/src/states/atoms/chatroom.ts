import {atom} from 'recoil'

export type UIChatMessage = {
    id: string
    from_username: string
    to_username: string
    text: string
}
export const chatMessagesAtom = atom<Record<string, UIChatMessage>>({
    key: 'chatMessages',
    default: {},
})


export type ChatList = {
    username_counts: Record<string, number>
}
export const chatListAtom = atom<ChatList>({
    key: 'chatList',
    default: {username_counts: {}}
})

export function resetUsernameCount(state: ChatList, username: string): ChatList {
    let count = 0
    let users = {...state.username_counts}
    // delete users[username]
    users = {
        ...users,
        [username]: count,
    }
    return {
        username_counts: users
    }
}

export function addUsernameCount(state: ChatList, username: string): ChatList {
    let count = (state.username_counts[username] || 0) + 1
    let users = {...state.username_counts}
    delete users[username]
    users = {
        [username]: count,
        ...users
    }
    return {
        username_counts: users
    }
}
