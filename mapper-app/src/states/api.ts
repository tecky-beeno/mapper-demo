export const {REACT_APP_API_ORIGIN} = process.env

export function postJSON(url: string, body: any, headers?: { 'X-User'?: string }) {
    return fetch(`${REACT_APP_API_ORIGIN}${url}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            ...headers
        },
        body: JSON.stringify(body),
    })
}
