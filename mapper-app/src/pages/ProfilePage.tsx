import {useRecoilState} from "recoil";
import {authAtom} from "../states/atoms";
import {Redirect} from "react-router-dom";
import React from "react";

export function ProfilePage() {
    const [auth, setAuth] = useRecoilState(authAtom)
    function logout() {
        setAuth(null)
    }
    if (!auth) {
        return <Redirect to="/login" />
    }
    return (
        <div>
            <h2>Profile Page</h2>
            <button onClick={logout}>logout</button>
        </div>
    )
}
