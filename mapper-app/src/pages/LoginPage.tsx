import { Redirect } from 'react-router-dom'
import { useRecoilState } from 'recoil'
import { useState } from 'react'
import { authAtom } from '../states/atoms'
import { postJSON } from '../states/api'

export function LoginPage() {
  const [auth, setAuth] = useRecoilState(authAtom)
  const [username, setUsername] = useState('')

  async function login() {
    const res = await postJSON('/users', { username })
    const data = await res.json()
    setAuth(data.user)
  }

  if (auth) {
    return <Redirect to="/profile" />
  }
  return (
    <div>
      <h2>Login Page</h2>
      <input
        type="text"
        onChange={e => setUsername(e.target.value)}
        value={username}
      />
      <button onClick={login}>login</button>
    </div>
  )
}
