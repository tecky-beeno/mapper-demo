import {Link, useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {REACT_APP_API_ORIGIN} from "../../states/api";
import {useRecoilState} from "recoil";
import {authAtom} from "../../states/atoms";
import {Redirect} from "react-router";
import {resetUsernameCount, chatListAtom} from "../../states/atoms/chatroom";

type User = {
    id: string
    username: string
}

export function ChatroomList() {
    const params = useParams<{ username?: string }>()
    const [auth] = useRecoilState(authAtom)
    const [chatList, setChatList] = useRecoilState(chatListAtom)
    useEffect(() => {
        async function loadUsers() {
            const res = await fetch(`${REACT_APP_API_ORIGIN}/users`)
            const data = await res.json()
            setChatList(state => {
                data.users.forEach((user: User) => {
                    state = resetUsernameCount(state, user.username)
                })
                return state
            })
        }

        loadUsers().catch(err => console.error('failed to load chat list:', err))
    }, [setChatList])
    if (!auth) {
        return <Redirect to='/login'/>
    }
    const users = Object.entries(chatList.username_counts)
    return (
        <div style={{flexGrow: 1}}>
            {users.length === 0 ? <p>no users yet</p> : users.map(([username, count]) => (
                <Link
                    onClick={() => {
                        setChatList(state => resetUsernameCount(state, username))
                    }}
                    key={username}
                    to={`/chat/${username}`}
                    style={{
                        backgroundColor:
                            username === params.username ? 'lightsalmon' : 'initial',
                        padding: '0.5em',
                        display: 'block',
                    }}
                >
                    {
                        username === auth.username ? 'Saved Messages' : username
                    }
                    {count > 0 ? `(${count})` : null}
                </Link>
            ))}
        </div>
    )
}
