import React from "react";
import {ChatroomDetail} from "./ChatroomDetail";
import {ChatroomList} from "./ChatroomList";

export function ChatroomPage() {
    return (
        <div>
            <h2>Chatroom Page</h2>
            <div style={{ display: 'flex', width: '100%' }}>
                <ChatroomList />
                <ChatroomDetail />
            </div>
        </div>
    )
}
