import {useParams} from 'react-router-dom'
import React, {useEffect, useState} from 'react'
import {REACT_APP_API_ORIGIN, postJSON} from '../../states/api'
import {useRecoilState} from "recoil";
import {authAtom} from "../../states/atoms";
import {Redirect} from "react-router";
import {chatMessagesAtom, UIChatMessage} from "../../states/atoms/chatroom";


export function ChatroomDetail() {
    const params = useParams<{ username?: string }>()
    const [text, setText] = useState('')
    const [auth] = useRecoilState(authAtom)
    const [chatMessages, setChatMessages] = useRecoilState(chatMessagesAtom)

    const relatedMessages = Object.values(chatMessages).filter(message => {
        if (!auth) {
            return false
        }
        return [[auth.username, params.username], [params.username, auth.username]]
            .some(([from, to]) => message.from_username === from && message.to_username === to)
    }).reverse()

    useEffect(() => {
        async function loadChats() {
            if (!auth) {
                return
            }
            if (!params.username) {
                console.log('missing username in params')
                return
            }
            const res = await fetch(`${REACT_APP_API_ORIGIN}/users/${params.username}/chat`, {
                headers: {
                    'X-User': auth.username
                }
            })
            const data = await res.json()
            console.log('chats', data)
            setChatMessages(messages => {
                data.messages.forEach((msg: UIChatMessage) => {
                    messages = {...messages, [msg.id]: msg}
                })
                return messages
            })
        }

        loadChats().catch(err => console.error('failed to load chats', err))
    }, [auth, params.username, setChatMessages])

    async function send() {
        if (!text) {
            return
        }
        if (!params.username) {
            return
        }
        if (!auth) {
            return
        }
        await postJSON(`/users/${params.username}/chat`, {text}, {'X-User': auth.username})
        setText('')
    }

    if (!auth) {
        return <Redirect to='/login'/>
    }

    return (
        <div style={{flexGrow: 3}}>
            {!params.username ? (
                <p>Get started by selecting a room on the left</p>
            ) : (
                <>
                    {params.username === auth.username
                        ? <h3>Saved Messages</h3>
                        : <h3> Chatting with {params.username}</h3>}
                    <input
                        type="text"
                        value={text}
                        onChange={e => setText(e.target.value)}
                    />
                    <button onClick={send}>send</button>
                    {relatedMessages.length === 0 ? <p>no messages in this room yet</p> : relatedMessages.map(message =>
                        <p>{message.from_username}: {message.text}</p>)}
                </>
            )}
        </div>
    )
}
