/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {useEffect} from 'react'
import {useRecoilState} from "recoil";
import {Auth, authAtom, socketAtom} from "../states/atoms";
import {socket} from "../states/socket.io";
import {addUsernameCount, chatListAtom, chatMessagesAtom, UIChatMessage} from "../states/atoms/chatroom";

export function SocketComponent() {
    const [socketState, setSocketState] = useRecoilState(socketAtom)
    const [_1, setChatMessages] = useRecoilState(chatMessagesAtom)
    const [_2, setChatList] = useRecoilState(chatListAtom)
    const [_3, setAuth] = useRecoilState(authAtom)
    useEffect(() => {
        if (socketState.connecting) {
            return;
        }
        if (socketState.ready) {
            return
        }
        if (socket.connected) {
            return;
        }
        socket.connect()
        setSocketState({connecting: true, ready: false})
        socket.on('connect', () => {
            console.log('connected to server')
            setSocketState({connecting: false, ready: true})
        })
        socket.on('disconnect', () => {
            console.log('disconnected from server')
            setSocketState({connecting: false, ready: false})
        })
        socket.on('chatMessage', (newMessage: UIChatMessage) => {
            setChatMessages(messages => {
                return {...messages, [newMessage.id]: newMessage}
            })
            let auth: Auth | null = null
            setAuth(state => {
                auth = state
                return state
            })
            setChatList(state => {
                let roomName = newMessage.from_username === auth?.username ? newMessage.to_username : newMessage.from_username
                return addUsernameCount(state, roomName);
            })
        })
    }, [setSocketState, setChatMessages, socketState])
    return <>
    </>
}
