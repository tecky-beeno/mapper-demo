import {useRecoilState} from "recoil";
import {authAtom} from "../states/atoms";
import {Link} from "react-router-dom";
import React from "react";

export function Menu() {
    const [auth] = useRecoilState(authAtom)
    const name = auth ? auth.username : 'guest'
    return (
        <div>
            <h2>Menu</h2>
            <p>hello, {name}</p>
            {auth ? (
                <>
                    <Link to="/profile">Profile</Link> <Link to="/chat">Chatroom</Link>
                </>
            ) : (
                <>
                    <Link to="/welcome">Welcome</Link> <Link to="/login">Login</Link>
                </>
            )}
        </div>
    )
}
