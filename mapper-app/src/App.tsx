import React, {useState, useEffect} from 'react'
import './App.css'
import {
    Redirect,
    Switch,
    Link,
    Route,
    Router,
    useParams,
} from 'react-router-dom'
import {createBrowserHistory} from 'history'
import {atom, useRecoilState, RecoilRoot} from 'recoil'
import {io,} from 'socket.io-client'
import {authAtom, socketAtom} from "./states/atoms";
import {LoginPage} from "./pages/LoginPage";
import {ChatroomPage} from './pages/ChatroomPage'
import {ProfilePage} from "./pages/ProfilePage";
import {Menu} from "./components/Menu";
import {socket} from "./states/socket.io";
import {SocketComponent} from "./components/SocketComponent";


const history = createBrowserHistory()


function Index() {
    return (
        <RecoilRoot>
            <Router history={history}>
                <App/>
            </Router>
        </RecoilRoot>
    )
}

function App() {
    const [auth] = useRecoilState(authAtom)
    const [socketState] = useRecoilState(socketAtom)
    useEffect(() => {
        if (socketState.ready) {
            socket.emit('auth', auth)
        }
    }, [auth, socketState.ready])
    const hasLogin = auth != null
    return (
        <div className="App">
            <SocketComponent/>
            <Menu/>
            <Switch>
                <Route path="/login" component={LoginPage}/>
                <Route path="/profile" component={ProfilePage}/>
                <Route path="/chat/:username" component={ChatroomPage}/>
                <Route path="/chat" component={ChatroomPage}/>
                <Route path="/">
                    {hasLogin ? <Redirect to="/profile"/> : <Redirect to="/login"/>}
                </Route>
            </Switch>
        </div>
    )
}

export default Index
